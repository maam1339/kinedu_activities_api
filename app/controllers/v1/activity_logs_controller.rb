module V1
  class ActivityLogsController < ApplicationController
    before_action :set_activity_log, only: %i[show update destroy]
    before_action :get_activitiy_logs, only: %i[index]

    def index
      render :index
    end

    def show
      render :show
    end

    def create
      @activity_log = ActivityLog.new(activity_log_params)

      if @activity_log.save
        render :show, status: :created
      else
        render json: @activity_log.errors, status: :unprocessable_entity
      end
    end

    def update
      if @activity_log.update(activity_log_params)
        render :show
      else
        render json: @activity_log.errors, status: :bad_request
      end
    end

    def destroy
      @activity_log.destroy
    rescue
      render(
        json: { message: I18n.t('activerecord.errors.messages.restrict_dependent_destroy.has_many') },
        status: :conflict
      )
    end

    private

    def set_activity_log
      @activity_log = ActivityLog.find(params[:id])
    rescue 
      render(
        json: { message: I18n.t('activerecord.exceptions.not_found') },
        status: :not_found
      )
    end

    def activity_log_params
      params
        .require(:activity_log)
        .permit(
          :baby_id,
          :assistant_id,
          :activity_id,
          :start_time,
          :stop_time,
          :comments
        )
    end

    # brings all the resources of the database, if the value :baby_id exists
    # in the parameters it brings all those that belong to baby according to
    # the association that exists between the two models
    def get_activitiy_logs
      if params[:baby_id]
        @activity_logs = activity_logs_for_baby
      else
        @activity_logs = ActivityLog.filter(params.slice(:baby, :assistant), [:baby, :activity, :assistant])
      end
    end

    # query to bring the activity logs according to a :baby_id param
    def activity_logs_for_baby
      ActivityLog
        .where(baby_id: params[:baby_id])
        .includes(:baby, :activity, :assistant)
    end
  end
end