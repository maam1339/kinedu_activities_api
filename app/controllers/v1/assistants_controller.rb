module V1
  class AssistantsController < ApplicationController
    before_action :set_assistant, only: %i[show update destroy]

    def index
      @assistants = Assistant.all

      render :index
    end

    def show
      render :show
    end

    def create
      @assistant = Assistant.new(assistant_params)

      if @assistant.save
        render :show, status: :created
      else
        render json: @assistant.errors, status: :unprocessable_entity
      end
    end

    def update
      if @assistant.update(assistant_params)
        render :show
      else
        render json: @assistant.errors, status: :bad_request
      end
    end

    def destroy
      @assistant.destroy
    rescue
      render(
        json: { message: I18n.t('activerecord.errors.messages.restrict_dependent_destroy.has_many') },
        status: :conflict
      )
    end

    private

    def set_assistant
      @assistant = Assistant.find(params[:id])
    rescue 
      render(
        json: { message: I18n.t('activerecord.exceptions.not_found') },
        status: :not_found
      )
    end

    def assistant_params
      params.require(:assistant).permit(:name, :group, :address, :phone)
    end
  end
end