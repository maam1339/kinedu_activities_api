module V1
  class ActivitiesController < ApplicationController
    before_action :set_activity, only: %i[show update destroy]

    def index
      @activities = Activity.all

      render :index
    end

    def show
      render :show
    end

    def create
      @activity = Activity.new(activity_params)

      if @activity.save
        render :show, status: :created
      else
        render json: @activity.errors, status: :unprocessable_entity
      end
    end

    def update
      if @activity.update(activity_params)
        render :show
      else
        render json: @activity.errors, status: :bad_request
      end
    end

    def destroy
      @activity.destroy
    rescue
      render(
        json: { message: I18n.t('activerecord.errors.messages.restrict_dependent_destroy.has_many') },
        status: :conflict
      )
    end

    private

    def set_activity
      @activity = Activity.find(params[:id])
    rescue 
      render(
        json: { message: I18n.t('activerecord.exceptions.not_found') },
        status: :not_found
      )
    end

    def activity_params
      params.require(:activity).permit(:name, :description)
    end
  end
end
