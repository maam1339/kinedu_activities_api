module V1
  class BabiesController < ApplicationController
    before_action :set_baby, only: %i[show update destroy]

    def index
      @babies = Baby.all

      render :index
    end

    def show
      render :show
    end

    def create
      @baby = Baby.new(baby_params)

      if @baby.save
        render :show, status: :created
      else
        render json: @baby.errors, status: :unprocessable_entity
      end
    end

    def update
      if @baby.update(baby_params)
        render :show
      else
        render json: @baby.errors, status: :bad_request
      end
    end

    def destroy
      @baby.destroy
    rescue
      render(
        json: { message: I18n.t('activerecord.errors.messages.restrict_dependent_destroy.has_many') },
        status: :conflict
      )
    end

    private

    def set_baby
      @baby = Baby.find(params[:id])
    rescue 
      render(
        json: { message: I18n.t('activerecord.exceptions.not_found') },
        status: :not_found
      )
    end

    def baby_params
      params
        .require(:baby)
        .permit(
          :name,
          :birthday,
          :mother_name,
          :father_name,
          :address,
          :phone
        )
    end
  end
end