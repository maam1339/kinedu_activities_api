json.array! @babies do |baby|
  json.partial! '/v1/babies/baby', baby: baby
end