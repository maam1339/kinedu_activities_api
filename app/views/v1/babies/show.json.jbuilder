json.baby do
  json.partial!(
    '/v1/babies/baby',
    baby: @baby
  )
end