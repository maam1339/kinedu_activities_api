json.call(
  activity_log,
  :id,
  :start_time,
  :stop_time,
  :duration,
  :comments,
  :created_at,
  :updated_at,
  # => objects
  :baby,
  :assistant,
  :activity
)