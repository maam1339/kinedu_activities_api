json.activity_log do
  json.partial!(
    '/v1/activity_logs/activity_log',
    activity_log: @activity_log
  )
end