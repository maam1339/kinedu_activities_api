json.array! @activity_logs do |activity_log|
  json.partial! '/v1/activity_logs/activity_log', activity_log: activity_log
end