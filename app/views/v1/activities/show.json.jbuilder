json.activity do
  json.partial!(
    '/v1/activities/activity',
    activity: @activity
  )
end