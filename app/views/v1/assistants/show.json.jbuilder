json.assistant do
  json.partial!(
    '/v1/assistants/assistant',
    assistant: @assistant
  )
end