json.array! @assistants do |assistant|
  json.partial! '/v1/assistants/assistant', assistant: assistant
end