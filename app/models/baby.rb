class Baby < ApplicationRecord
  # => associations
  has_many :activity_logs, dependent: :destroy
  has_many :assistants, through: :activity_logs, dependent: :destroy
  has_many :activities, through: :activity_logs, dependent: :destroy

  # => validadtions
  validates :name, :birthday, :mother_name, :father_name, presence: true
end
