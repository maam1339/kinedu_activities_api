class Assistant < ApplicationRecord
  # => associations
  has_many :activity_logs, dependent: :nullify
  has_many :babies, through: :activity_logs, dependent: :destroy
  has_many :activities, through: :activity_logs, dependent: :destroy

  # => validations
  validates :name, :group, presence: true
end
