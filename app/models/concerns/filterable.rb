module Filterable
  # Filter one-to-many relationships. According to the
  # past values, build a query and execute it, obtaining
  # a new collection.
  #
  # Example:
  #
  # Model.filter params.slice(:actor, :user)
  #
  # You can also include an array with some resources
  # that you want to include in the query according
  # with an association.
  #
  # Example:
  #
  # Model.filter params.slice(:actor, :user), [:father, :comments]
  #
  # NOTICE: This method needs to be worked more so that it
  # can fit in all the models. There is still a dependency
  # on models that contain the parameter :name since it will
  # only filter on that parameter.
  #
  def filter(params={}, includes=[])
    collection = self.includes(includes).where(nil)

    params.each do |key, value|
      collection = collection.where(
        key.pluralize.to_sym => { name: sanitize(value) }
      )
    end

    collection
  end

  private 

  def sanitize(string)
    string.gsub(/[^a-zA-Z\d\s:-áéíóú]/, ' ')
  end
end