class ActivityLog < ApplicationRecord
  extend Filterable

  # => callback
  before_save :calculate_duration, if: :stop_time?

  # => associations
  belongs_to :baby
  belongs_to :assistant
  belongs_to :activity

  # => validations
  validates_associated :baby, :assistant, :activity
  validates :start_time, presence: true
  validate :times_are_consistent, if: :stop_time?, on: :update

  private

  # calculates the duration of the activity based
  # on a start time and an end time
  def calculate_duration
    self.duration = ((stop_time - start_time) / 60).round
  end

  # adds an error to the record if the start time
  # exceeds the end time of the activity
  def times_are_consistent
    if start_time > stop_time
      errors.add(
        :stop_time,
        I18n.t('errors.messages.inconsistent_times')
      )
    end
  end
end
