class Activity < ApplicationRecord
  # => associations
  has_many :activity_logs, dependent: :restrict_with_exception
  has_many :babies, through: :activity_logs
  has_many :assistants, through: :activity_logs

  # => validations
  validates :name, :description, presence: true
end
