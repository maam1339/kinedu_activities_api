# Kinedu Activities API

API to keep track of the activities that babies nursery school care.
-- TEST FOR KINEDU COMPANY --

### I18n internationalization

The application is configured to handle Spanish, English and Portuguese. If you want to specify the answer of the application in a specific language you only have to pass the variable "Accept-Language" in the headers ("en" for English, "es" for Spanish and "pt" for Portuguese).

## Code Status

[![CircleCI](https://circleci.com/bb/maam1339/kinedu_activities_api.svg?style=shield)](https://circleci.com/bb/maam1339/kinedu_activities_api)

## Software specifications

- Ruby 2.5.1
- Rails 5.2.1
- MySql

## Installation specifications

### RVM set application versions

```
rvm install 2.5.1
```

```
rvm use ruby-2.5.1@rails5.2.1 --create
```

```
gem install rails --version=5.2.1
```

check the versions

```
ruby -v
rails -v
```

### Download and run the application

```
git clone https://maam1339@bitbucket.org/maam1339/kinedu_activities_api.git
```

```
cd kinedu_activities_api
```

Install dependencies

```
bundle install
```

Run the server

```
rails server
```

### Resources and documentation

- Trello board: https://trello.com/b/N5aDDpXi
- Documentation of access points: https://documenter.getpostman.com/view/2691667/RWaF2BgL
- API Application: https://bitbucket.org/maam1339/kinedu_activities_api/
- User interface: https://bitbucket.org/maam1339/kinedu-activities-ui/

## Credits 

[Armando Alejandre](http://armando-alejandre.herokuapp.com/)

# Enjoy friends!
