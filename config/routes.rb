Rails.application.routes.draw do

  # => V1 endpoints || routes
  namespace :v1, defaults: {format: :json} do
    resources :activities
    resources :activity_logs
    resources :assistants
    resources :babies do
      resources :activity_logs, only: [:index]
    end
  end
end
